package br.edu.up.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import br.edu.up.app.data.DadosAdapter
import br.edu.up.app.data.Item
import br.edu.up.app.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Aborgagem declarativa
        val binding = ActivityMainBinding.inflate(layoutInflater)
        //binding.meuTextView.text = "Olá mundo!"

        val dados = mutableListOf<Item>()
        for(i in 1..100){
            //val itemNovo: Item = Item("Item $i", "SubItem $i")
            //dados.add(itemNovo)
            dados.add(Item("Item $i", "SubItem $i"))
        }

        binding.lista.adapter = DadosAdapter(dados)
        setContentView(binding.root)

        //Abordagem imperativa
        //setContentView(R.layout.activity_main)
        //val meuTexto = findViewById<TextView>(R.id.meuTextView)
        //meuTexto.text = "Olá mundo!"
    }
}