package br.edu.up.app.data

data class Item(
    val titulo: String,
    val subTitulo: String
)